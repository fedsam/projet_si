package fr.polytech.larynxapp.model;

/**
 * User/patient entity,
 */
public class User {

    /**
     * The id of user
     * Primary Key
     */
    private int userId;

    /**
     * The name of the user.
     */
    private String username;


    /**
     * User default builder.
     *
     * username will be empty.
     */
    public User(){
        username = "";
    }

    /**
     * Constructor with 1 parameters.
     *
     * @param username the username of the user
     */
    public User(String username){
        this.username = username;
    }

    /**
     * get userid
     * @return the id of the user
     */
    public int getUserId() {
        return userId;
    }

    /**
     * set userid
     * @param userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * Getter for the username of the user.
     *
     * @return the username of the user
     */
    public String getUserName() {
        return username;
    }

    /**
     * Setter for the username of the user.
     *
     * @param username the new username for the user
     */
    public void setUserName( String username ) {
        this.username = username;
    }

    /**
     * ToString method
     */
    @Override
    public String toString() { return username;}


}
